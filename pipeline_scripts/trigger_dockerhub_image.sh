#!/bin/bash

# Argument 1 = docker hub tag name to be triggered (eg: > trigger_dockerhub_image.sh centos6_base)

# You must set MOODLEMT_DOCKERHUB_TRIGGER_TOKEN environment var into your Bitbucket root account (https://bitbucket.org/account/user/{your-user-id}/addon/admin/pipelines/account-variables), with the Trigger Token generated in Docker Hub

JSONDATA="{\"docker_tag\":\"${1}\"}"
curl -H "Content-Type: application/json" --data $JSONDATA -X POST https://registry.hub.docker.com/u/moodlemt/moodlemt_docker_stacks/trigger/$MOODLEMT_DOCKERHUB_TRIGGER_TOKEN/
