#!/bin/bash -x

set -e

if [ "$ENABLE_DEVEL" = "yes" ] ; then
	[ -f "/etc/php56debug.ini" ] && yes | cp /etc/php56debug.ini /etc/php.ini
else
	[ -f "/etc/php56.ini" ] && yes | cp /etc/php56.ini /etc/php.ini
fi

XDEB_FILE_ORIG=/etc/php.d/90-xdebug.ini.hid
XDEB_FILE_DEST=/etc/php.d/90-xdebug.ini.hidden
if [ "$ENABLE_DEVEL" = "yes" ] ; then
	XDEB_FILE_DEST=/etc/php.d/90-xdebug.ini
fi
# Tras la creacion, los restantes inicios ya tienen el fichero en el XDEB_FILE_DEST, por lo que hay que comprobar que exista el XDEB_FILE_ORIG para que no fallen los reinicios subsiguientes.
[ -f "$XDEB_FILE_ORIG" ] && mv $XDEB_FILE_ORIG $XDEB_FILE_DEST

[ ! -z "$HOST_IP" ] && sed -i "s/;xdebug\.remote_host=HOST_IP/xdebug\.remote_host=$HOST_IP/g" $XDEB_FILE_DEST

# Se permite montar con -v el php.ini deseado en /data/sw_confs/php/php.ini
if [ -f "/data/sw_confs/php/php.ini" ] ; then
	cp /data/sw_confs/php/php.ini /etc/php.ini
fi
exec "$@"
