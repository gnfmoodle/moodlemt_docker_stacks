# Introduction

## What is Moodle?

[Moodle](https://moodle.org) is a learning platform designed to provide educators, administrators and learners with a single robust,
secure and integrated system to create personalized learning environments.

## What is Multitenancy?

The term **software multitenancy** refers to a software architecture in which a single instance of software runs on a server (or cluster) and serves
multiple tenants. A tenant is a group of users who share a common access with specific privileges to the software instance. With a multitenant
architecture, a software application is designed to provide every tenant a dedicated share of the instance - including its data, configuration,
user management, tenant individual functionality and non-functional properties. Multitenancy contrasts with multi-instance architectures, where
separate software instances operate on behalf of different tenants.

Multitenancy is a decisive feature in the **cloud computing** paradigm, very strongly in the **SaaS** model. Examples of web applications under a 
multitenant architecture are: the enterprise version of Gmail / Drive, Office 365, ...

[Moodle-mt](https://bitbucket.org/moodle_aws/moodle_mt) is a project whose main purpose is to adapt the source code of the standard Moodle
distribution so that it can be offered as a web service under a SaaS model hosted on Amazon Web Services.

## What is Docker?

[Docker](https://www.docker.com/) is a software virtualization project whose main purpose is to streamline and automate the development and
deployment of applications software. It provides a layer of abstraction and virtualization that allows you to run Linux operating systems on
multiple host operating systems (for now, the hosts can be: other LINUX, Windows and Mac).

Agility is obtained through the concepts of images and containers. An image is a set of shell commands that extend the software of a LINUX base
image. Compiling an image results in a container, which is the component that can be run on the virtualizer.
The images can be published in Docker Hub, the official public repository, so that anyone can quickly combine various containers based on public
images (apache, mysql, redis, etc.) to build the platform on which to run their applications.

## About this Moodle Docker Stack

This repository contains the appropriate Dockerfiles to build a suitable platform stack to support various versions of Moodle (2.X and 3.X for
now) by combining the containers of the appropriate versions of Operating System, Apache, PHP, and MySQL.

This stack of software follows the philosophy of the Moodle-mt project in which security is paramount. Therefore, the stack is designed to work
only on HTTPs and allows you to configure an isolated private network for their containers to communicate with each other.

This stack is published in DockerHub under the [moodlemt/moodlemt_docker_stacks](https://hub.docker.com/r/moodlemt/moodlemt_docker_stacks/) repository.

# How to Use this Moodle Docker Stack

This repository offers the following hierarchy of images:

![Images Stack](/resources/imgs/imgs-stack.png =570x)

## Running single containers

### Example 1 (insecure)
Standard Moodle for developing running over our default domain (https://default-localhost):

  * Host system: Linux Red Hat / CentOS / Fedora
  * Docker stack: PHP56 + MySQL56
  * Moodle version: 3.2
  * Git version: 1.7 or newer
  * Session variables: 

```bash    
cd /data01/html
git clone -b MOODLE_32_STABLE --single-branch https://github.com/moodle/moodle.git moodle

# 1) network for stack communication features
docker network create --driver bridge moodlenet
# 2) web server container
# WARNING: Paths of files to be mounted must exist in the host system
# NOTE: for your custom php.ini => -v [host-path-my-php.ini]:/data/sw_confs/php/php.ini
docker run -d -p 80:80 -p 443:443 \
    -v /etc/localtime:/etc/localtime \
    -v /data01/html/moodle:/var/www/html \
    -v /data01/docker_shared_vols/sw_confs:/data/sw_confs \
    -v /data01/moodle_data/data:/data/moodle_data \
    -e ENABLE_DEVEL=yes \
    --restart=unless-stopped --network=moodlenet \
    --name=web moodlemt/moodlemt_docker_stacks:php55_apache22
# 3) db server container
# WARNING: Paths of files to be mounted must exist in the host system
docker run -d -p 3306:3306 \
    -v /etc/localtime:/etc/localtime \
    -v /data01/mysql:/var/lib/mysql \
    -v /data01/docker_shared_vols/logs/mysqld.error.log:/var/log/mysqld.error.log \
    -v /data01/docker_shared_vols/logs/mysqld.general.log:/var/log/mysqld.general.log \
    -v /data01/docker_shared_vols/logs/mysqld.slowq.log:/var/log/mysqld.slowq.log \
    -e ENABLE_DEVEL=yes \
    -e MYSQL_ALLOW_EMPTY_PASSWORD=yes \
    -e MYSQL_ROOT_HOST="172.16.0.0/255.240.0.0" \
    --restart=unless-stopped --network=moodlenet \
    --name=mysql moodlemt/moodlemt_docker_stacks:mysql56

# Next lines for shell session in containers
docker exec -i -t web /bin/bash
docker exec -i -t mysql /bin/bash

# In the Moodle config.php: $CFG->dbhost = "mysql"; $CFG->dbuser = "root"; $CFG->dbpass = "";

# To launch Moodle installation from host system:
#   a) Get the web container IP; it depends on the host net
docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' web
#   b) Map our default domain (default-localhost)
echo "" >> /etc/hosts
echo "[web-container-ip] default-localhost" >> /etc/hosts
#   c) Open a browser and go to https://default-localhost
```

### Example 2 (improved security)
Same stack with improved security and debug disabled (more suitable for production):
```
docker network create moodlenet
docker run -d -p 80:80 -p 443:443 \
    -v /etc/localtime:/etc/localtime \
    -v /data01/html/moodle:/var/www/html \
    --restart=unless-stopped --network=moodlenet \
    --name=web moodlemt/moodlemt_docker_stacks:php55_apache22
docker run -d -p 3306:3306 \
    -v /etc/localtime:/etc/localtime \
    -v /data01/docker_shared_vols/logs/mysqld.error.log:/var/log/mysqld.error.log \
    -v /data01/docker_shared_vols/logs/mysqld.slowq.log:/var/log/mysqld.slowq.log \
    -e MYSQL_ALLOW_EMPTY_PASSWORD=yes \
    -e MYSQL_ROOT_HOST="172.16.0.0/255.240.0.0" \
    --restart=unless-stopped --network=moodlenet \
    --name=mysql moodlemt/moodlemt_docker_stacks:mysql56
```

### Example 3 (same as eg 2, but in Windows PowerShell)
You can set your localtime by copying next localtime-Madrid file from LINUX environment (/usr/share/zoneinfo/Europe/Madrid).
```
docker network create moodlenet
docker run -d -p 80:80 -p 443:443 `
	-v c:/data01/docker_local_confs/system/localtime-Madrid:/etc/localtime `
    -v c:/data01/html/moodle:/var/www/html `
    --restart=unless-stopped --network=moodlenet `
    --name=web moodlemt/moodlemt_docker_stacks:php56_apache22
docker run -d -p 3306:3306 `
    -v c:/data01/docker_local_confs/system/localtime-Madrid:/etc/localtime `
    -v c:/data01/docker_shared_vols/logs/mysqld.error.log:/var/log/mysqld.error.log `
    -v c:/data01/docker_shared_vols/logs/mysqld.slowq.log:/var/log/mysqld.slowq.log `
    -e MYSQL_ALLOW_EMPTY_PASSWORD=yes `
    -e MYSQL_ROOT_HOST="172.16.0.0/255.240.0.0" `
    --restart=unless-stopped --network=moodlenet `
    --name=mysql moodlemt/moodlemt_docker_stacks:mysql56
```

### Example 4 (same as eg 2, but in PowerShell and for debugging with IDEs in Windows)
With Docker, Xdebug "[multiple developers mode](https://xdebug.org/docs/remote)" (unknown client IP) is not supported easily because of host IP rewriting in communications between the host system and the container.
Instead of that, the "**single developer mode**" is supported in our PHP images via the **HOST_IP env arg**, setting it into xdebug.remote_host.
```
<# Next lines recover the Windows (host system) IP #>
$configs=Get-WmiObject win32_NetworkAdapterConfiguration -Filter Index=7 | Select IPAddress
$hostip=$configs[0].IPAddress[0]
Write-Host "$yourtargetIP"

<# Next lines for launching containers. Special attention to HOST_IP env var #>
docker network create moodlenet
docker run -d -p 80:80 -p 443:443 `
	-v c:/data01/docker_local_confs/system/localtime-Madrid:/etc/localtime `
    -v c:/data01/html/moodle:/var/www/html `
    -e ENABLE_DEVEL=yes -e HOST_IP=$hostip `
    --restart=unless-stopped --network=moodlenet `
    --name=web moodlemt/moodlemt_docker_stacks:php56_apache22
docker run -d -p 3306:3306 `
    -v c:/xampp_htdocs/docker_local_confs/system/localtime:/etc/localtime:ro `
    -v c:/xampp_htdocs/docker_local_volumes/moodle_db_data/:/var/lib/mysql/ `
    -v c:/xampp_htdocs/docker_local_confs/mysql/mysql56/config-file.cnf:/etc/mysql/conf.d/config-file.cnf `
    -v c:/xampp_htdocs/docker_local_volumes/logs/mysql_error.log:/var/log/mysql_error.log `
    -v c:/xampp_htdocs/docker_local_volumes/logs/mysql_general.log:/var/log/mysql_general.log `
    -v c:/xampp_htdocs/docker_local_volumes/logs/mysql_slowq.log:/var/log/mysql_slowq.log `
    -e MYSQL_ROOT_PASSWORD=123 --network=moodlenet --name=mdl_mysql mysql:5.6
```
Additionally, if you want to use your IDE (eg Netbeans) to debug with Xdebug, you must configure the port it is going to use (usually 9000). Therefore, you will have to **open port 9000 in the Windows firewall**; opening it to all sources avoids change the rule if you ever change the virtual network mask used by docker (usually 10.0.75.0/24).

### Example 5 (same as eg 4, but in Mac)
In Mac container cannot comunicate with host via host IP. Instead, you can use the special DNS name **docker.for.mac.localhost** (only available in Mac).
```
hostname="docker.for.mac.localhost"
[...]
docker run -d -p 80:80 -p 443:443 \
	-v c:/data01/docker_local_confs/system/localtime-Madrid:/etc/localtime \
    -v c:/data01/html/moodle:/var/www/html \
    -e ENABLE_DEVEL=yes -e HOST_IP=$hostname \
    --restart=unless-stopped --network=moodlenet \
    --name=web moodlemt/moodlemt_docker_stacks:php56_apache22
[...]
```

## With docker compose
TODO Explicar estructura de los directorios base que se informan mediante ENV VARS
En LINUX todas las variables de entorno usadas pueden fijarse en /etc/profile.d/gnfdocker.csh y /etc/profile.d/gnfdocker.sh.
En Windows, pueden fijarse como variables del sistema del usuario (XXXXXXXXXXXXXXXXXXXX).